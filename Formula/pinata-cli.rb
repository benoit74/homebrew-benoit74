class PinataCli < Formula
  desc "Pinata command-line tool"
  homepage "https://gitlab.com/benoit74/pinata-cli"
  url "https://gitlab.com/benoit74/pinata-cli.git",
    :tag      => "0.2.1",
    :revision => "7736b729f3df69c6861a74f5f11919a592faa0be"

  depends_on "go" => :build

  def install
    system "go", "build", "-o", bin/"pinata", "./pinata"

    # bash_completion.install "scripts/cheat.bash"
    # fish_completion.install "scripts/cheat.fish"
  end

  test do
    assert_match "pinata version #{version}", shell_output("#{bin}/pinata --version")
  end
end
